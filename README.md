# Simple SQLite3 Wrapper

* [Features](#markdown-header-major-features)
* [Setup](#markdown-header-setup)
* [Examples](#markdown-header-examples)
* [API Overview](#markdown-header-api-overview)
* [API Documentation](#markdown-header-api-documentation)
* [License](#markdown-header-license)

## Major Features ##

* Small and easy to use - *one line prepared statements*
* Includes sqlite3 as it is public domain - *you only have to download this*
* Errors get recorded - *no try-catch-blocks or checking every step is needed*
* sqlite.h is not included - *IDEs does not (have to) parse; no overload of unused functions*
* STL-like container layout - *for easy usage with STL functions*

## Not featured / Disadvantages ##

* sqlite3 is not completely covered
* Complex usage of sqlite3 have to be included by yourself
* Multithreading not tested

## Setup ##

* Download this
* Add the following files to your project:
      * sqlite3.c - sqlite3 amalgamation source
      * sqlite3.h - sqlite3 header
      * ssqlite3.cpp - SSQLite source
      * ssqlite3.hpp - SSQLite3 header
* Note: the 4 files have to be in the same directory
* Try the examples
* If you like, give credit

## Examples ##

Connecting and disconnecting

```
SSQLite3::Connection db;
db.SetLogging( true ); // activate for debugging purpose
db.Connect( "database.db" );

// query a lot

db.Disconnect( );
```

Queries

```
db.Execute( "CREATE TABLE foobar( KEY TEXT, VALUE TEXT )" );
db.Execute( SSQLite3::Statement( "INSERT INTO foobar(KEY,VALUE) VALUES( ?, ? )" ) <= "foo" <= "bar" );
SSQLite3::Result result = db.Execute( "SELECT * FROM foobar ORDER BY KEY ASC" );
```

Handle Results

```
SSQLite3::Result res = // returned from any query
if ( res ) // was the query successful?
    // work with it
```

```
SSQLite3::Result res = // returned from INSERT/UPDATE/DELETE query
int rows = res.AffectedRows( );
```

```
SSQLite3::Result res = // returned from SELECT query
for ( SSQLite3::Result::iterator rows( res.begin( ) ) ; rows != res.end( ) ; ++rows )
    for ( SSQLite3::Result::iterator entries( rows->begin( ) ) ; entries != rows->end( ) ; ++entries )
        // name: entries->first
        // value: entries->second
```

## API Overview ##

Notice: I removed the SSQLite3:: namespace-prefix in all cases so imagine it for yourself or use it with using namespace SSQLite3; I also hide basic constructor if they are not mentionable.

* [Functions](#markdown-header-public-functions)
* [Object](#markdown-header-object)
* [ObjectMap](#markdown-header-objectmap)
* [Statement](#markdown-header-statement)
* [Result](#markdown-header-result)
* [Connection](#markdown-header-connection)

### Public Functions ###

* std::string [Formatq](#markdown-header-ssqlite3-format)( const std::string& str )
* std::string [FormatQ](#markdown-header-ssqlite3-format)( const std::string& str )
* std::string [Formatz](#markdown-header-ssqlite3-format)( const std::string& str )

### Object ###

* [Object](#markdown-header-ssqlite3-object-constructor)
* [Object::Type](#markdown-header-ssqlite3-object-type)
* [Object::Object](#markdown-header-ssqlite3-object-constructor)( )
* [Object::Object](#markdown-header-ssqlite3-object-constructor)( const [Object](#markdown-header-ssqlite3-object-constructor)& value )
* [Object::Object](#markdown-header-ssqlite3-object-constructor)( const int& value )
* [Object::Object](#markdown-header-ssqlite3-object-constructor)( const double& value )
* [Object::Object](#markdown-header-ssqlite3-object-constructor)( const std::string& value )
* [Object::Object](#markdown-header-ssqlite3-object-constructor)( const char* value, const std::size_t& size )
* [Object::~Object](#markdown-header-ssqlite3-object-destructor)( )
* [Object::Type](#markdown-header-ssqlite3-object-type) [Object::GetType](#markdown-header-ssqlite3-object-type-checking)( ) const
* bool [Object::IsInt](#markdown-header-ssqlite3-object-type-checking)( ) const
* bool [Object::IsReal](#markdown-header-ssqlite3-object-type-checking)( ) const
* bool [Object::IsText](#markdown-header-ssqlite3-object-type-checking)( ) const
* bool [Object::IsBlob](#markdown-header-ssqlite3-object-type-checking)( ) const
* bool [Object::IsNothing](#markdown-header-ssqlite3-object-type-checking)( ) const
* int [Object::GetInt](#markdown-header-ssqlite3-object-retrieve-value)( ) const
* double [Object::GetReal](#markdown-header-ssqlite3-object-retrieve-value)( ) const
* std::string [Object::GetText](#markdown-header-ssqlite3-object-retrieve-value)( ) const
* const char* [Object::GetBlob](#markdown-header-ssqlite3-object-retrieve-value)( ) const
* std::size_t [Object::GetBlobSize](#markdown-header-ssqlite3-object-retrieve-value)( ) const
* Object::[operator int](#markdown-header-ssqlite3-object-retrieve-value)( ) const
* Object::[operator std::string](#markdown-header-ssqlite3-object-retrieve-value)( )
* Object::[operator double](#markdown-header-ssqlite3-object-retrieve-value)( ) const
* void [Object::SetInt](#markdown-header-ssqlite3-object-assign-value)( const int& value )
* void [Object::SetReal](#markdown-header-ssqlite3-object-assign-value)( const double& value )
* void [Object::SetText](#markdown-header-ssqlite3-object-assign-value)( const std::string& value )
* void [Object::SetText](#markdown-header-ssqlite3-object-assign-value)( const char* value )
* void [Object::SetBlob](#markdown-header-ssqlite3-object-assign-value)( const char* value, const std::size_t& size )
* [Object](#markdown-header-ssqlite3-object-constructor)& [Object::operator =](#markdown-header-ssqlite3-object-assign-value) ( const [Object](#markdown-header-ssqlite3-object-constructor)& value )
* [Object](#markdown-header-ssqlite3-object-constructor)& [Object::operator =](#markdown-header-ssqlite3-object-assign-value) ( const int& value )
* [Object](#markdown-header-ssqlite3-object-constructor)& [Object::operator =](#markdown-header-ssqlite3-object-assign-value) ( const char * value )
* [Object](#markdown-header-ssqlite3-object-constructor)& [Object::operator =](#markdown-header-ssqlite3-object-assign-value) ( const std::string& value )
* [Object](#markdown-header-ssqlite3-object-constructor)& [Object::operator =](#markdown-header-ssqlite3-object-assign-value) ( const double& value )

### ObjectMap ###

* [ObjectMap::iterator](#markdown-header-ssqlite3-objectmap-iterator)
* [ObjectMap::const_iterator](#markdown-header-ssqlite3-objectmap-iterator)
* [ObjectMap::iterator](#markdown-header-ssqlite3-objectmap-iterator) [ObjectMap::begin](#markdown-header-ssqlite3-objectmap-iterator)( )
* [ObjectMap::const_iterator](#markdown-header-ssqlite3-objectmap-iterator) [ObjectMap::begin](#markdown-header-ssqlite3-objectmap-iterator)( ) const
* [ObjectMap::iterator](#markdown-header-ssqlite3-objectmap-iterator) [ObjectMap::end](#markdown-header-ssqlite3-objectmap-iterator)( )
* [ObjectMap::const_iterator](#markdown-header-ssqlite3-objectmap-iterator) [ObjectMap::end](#markdown-header-ssqlite3-objectmap-iterator)( ) const
* bool [ObjectMap::empty](#markdown-header-ssqlite3-objectmap-iterator)( ) const
* [Object](#markdown-header-ssqlite3-object-constructor)& [ObjectMap::operator](#markdown-header-ssqlite3-objectmap-access)[]( const std::string& key )
* [Object](#markdown-header-ssqlite3-object-constructor)& [ObjectMap::At](#markdown-header-ssqlite3-objectmap-access)( const std::string& key )
* const [Object](#markdown-header-ssqlite3-object-constructor) [ObjectMap::operator](#markdown-header-ssqlite3-objectmap-access) [] ( const std::string& key ) const
* const [Object](#markdown-header-ssqlite3-object-constructor) [ObjectMap::At](#markdown-header-ssqlite3-objectmap-access)( const std::string& key ) const
* bool [ObjectMap::operator](#markdown-header-ssqlite3-objectmap-exists) ( ) ( const std::string& key ) const
* bool [ObjectMap::Exists](#markdown-header-ssqlite3-objectmap-exists)( const std::string& key ) const

### Statement ###

* [Statement::Statement](#markdown-header-ssqlite3-statement-constructor)( const std::string& base )
* [Statement](#markdown-header-ssqlite3-statement-constructor)& [Statement::operator <<](#markdown-header-ssqlite3-statement-constructor) ( const T& value )
* [Statement](#markdown-header-ssqlite3-statement-constructor)& [Statement::operator <=](#markdown-header-ssqlite3-statement-bind) ( const T& value )
* [Statement](#markdown-header-ssqlite3-statement-constructor)& [Bind](#markdown-header-ssqlite3-statement-bind)( const char* value )
* [Statement](#markdown-header-ssqlite3-statement-constructor)& [Bind](#markdown-header-ssqlite3-statement-bind)( const std::string& value )
* [Statement](#markdown-header-ssqlite3-statement-constructor)& [Bind](#markdown-header-ssqlite3-statement-bind)( const int& value )
* [Statement](#markdown-header-ssqlite3-statement-constructor)& [Bind](#markdown-header-ssqlite3-statement-bind)( const double& value )
* [Statement](#markdown-header-ssqlite3-statement-constructor)& [Bind](#markdown-header-ssqlite3-statement-bind)( const Object& value )

### Result ###

* [Result::iterator](#markdown-header-ssqlite3-result-iterator)
* [Result::const_iterator](#markdown-header-ssqlite3-result-iterator)
* [Result::iterator](#markdown-header-ssqlite3-result-iterator) [Result::begin](#markdown-header-ssqlite3-result-iterator)( )
* [Result::const_iterator](#markdown-header-ssqlite3-result-iterator) [Result::begin](#markdown-header-ssqlite3-result-iterator)( ) const
* [Result::iterator](#markdown-header-ssqlite3-result-iterator) [Result::end](#markdown-header-ssqlite3-result-iterator)( )
* [Result::const_iterator](#markdown-header-ssqlite3-result-iterator) [Result::end](#markdown-header-ssqlite3-result-iterator)( ) const
* [Result::operator](#markdown-header-ssqlite3-result-check) bool( ) const
* int [Result::AffectedRows](#markdown-header-ssqlite3-result-check)( ) const

### Connection ###

* [Connection::Connection](#markdown-header-ssqlite3-connection-connection)( )
* [Connection::~Connection](#markdown-header-ssqlite3-connection-connection)( )
* bool [Connection::Connect](#markdown-header-ssqlite3-connection-connection)( const std::string& file, const bool& write = true, const bool& create = true )
* bool [Connection::CreateInMemory](#markdown-header-ssqlite3-connection-connection)( )
* bool [Connection::IsConnected](#markdown-header-ssqlite3-connection-connection)( ) const
* bool [Connection::Disconnect](#markdown-header-ssqlite3-connection-connection)( const bool& forceZombie = false )
* void [Connection::SetLogging](#markdown-header-ssqlite3-connection-log)( const bool& value )
* bool [Connection::IsLogging](#markdown-header-ssqlite3-connection-log)( ) const
* bool [Connection::HasMessage](#markdown-header-ssqlite3-connection-log)( ) const
* std::string [Connection::PopMessage](#markdown-header-ssqlite3-connection-log)( )
* std::string [Connection::PeekMessage](#markdown-header-ssqlite3-connection-log)( ) const
* Result [Connection::Execute](#markdown-header-ssqlite3-connection-execute)( const std::string& query )
* Result [Connection::Execute](#markdown-header-ssqlite3-connection-execute)( const Statement& query )

## API Documentation ##

### SSQLite3 Format ###

* std::string [Formatq](#markdown-header-ssqlite3-format)( const std::string& str )
* std::string [FormatQ](#markdown-header-ssqlite3-format)( const std::string& str )
* std::string [Formatz](#markdown-header-ssqlite3-format)( const std::string& str )

These functions take a string and converts them in the style of the formating tokens %q, %Q, %z introduced by sqlite3 and used in [sqlite3_printf-like](http://www.sqlite.org/c3ref/mprintf.html) functions.

### SSQLite3 Object Constructor ###

* [Object](#markdown-header-ssqlite3-object-constructor)
* [Object::Object](#markdown-header-ssqlite3-object-constructor)( )
* [Object::Object](#markdown-header-ssqlite3-object-constructor)( const [Object](#markdown-header-ssqlite3-object-constructor)& value )
* [Object::Object](#markdown-header-ssqlite3-object-constructor)( const int& value )
* [Object::Object](#markdown-header-ssqlite3-object-constructor)( const double& value )
* [Object::Object](#markdown-header-ssqlite3-object-constructor)( const std::string& value )
* [Object::Object](#markdown-header-ssqlite3-object-constructor)( const char* value, const std::size_t& size )

The Object class can hold the 4 major [types](#markdown-header-ssqlite3-object-type) used in the sqlite3 table definition: integer, real (floating) numbers, text, BLOBs(Binary Large Object Buffers). It can also hold the value NULL but it is called NOTHING due to complications with the macro NULL. For each of the 4 types including NOTHING is a constructor.

### SSQLite3 Object Destructor ###

* [Object::~Object](#markdown-header-ssqlite3-object-destructor)( )

The desctructor disposes automatically the used ressources so you do not have to care about freeing the space used behind.

### SSQLite3 Object Type ###

* [Object::Type](#markdown-header-ssqlite3-object-type)( )

Type is an enumeration of the 5 types the [Object](#markdown-header-ssqlite3-object-constructor) class can hold.

* REAL
* INTEGER
* BLOB
* TEXT
* NOTHING

### SSQLite3 Object Type Checking ###

* [Object::Type](#markdown-header-ssqlite3-object-type) [Object::GetType](#markdown-header-ssqlite3-object-type-checking)( ) const
* bool [Object::IsInt](#markdown-header-ssqlite3-object-type-checking)( ) const
* bool [Object::IsReal](#markdown-header-ssqlite3-object-type-checking)( ) const
* bool [Object::IsText](#markdown-header-ssqlite3-object-type-checking)( ) const
* bool [Object::IsBlob](#markdown-header-ssqlite3-object-type-checking)( ) const
* bool [Object::IsNothing](#markdown-header-ssqlite3-object-type-checking)( ) const

These functions serve the type checking purpose. The default [type](#markdown-header-ssqlite3-object-type) ist NOTHING.

### SSQLite 3 Object Retrieve Value ###

* int [Object::GetInt](#markdown-header-ssqlite3-object-retrieve-value)( ) const
* double [Object::GetReal](#markdown-header-ssqlite3-object-retrieve-value)( ) const
* std::string [Object::GetText](#markdown-header-ssqlite3-object-retrieve-value)( ) const
* const char* [Object::GetBlob](#markdown-header-ssqlite3-object-retrieve-value)( ) const
* std::size_t [Object::GetBlobSize](#markdown-header-ssqlite3-object-retrieve-value)( ) const

* Object::[operator int](#markdown-header-ssqlite3-object-retrieve-value)( ) const
* Object::[operator std::string](#markdown-header-ssqlite3-object-retrieve-value)( )
* Object::[operator double](#markdown-header-ssqlite3-object-retrieve-value)( ) const

All functions return always a value. If it is not the right type it will return a default value:

* TEXT = empty string("")
* BLOB = null pointer and size of 0
* REAL = Not A Number(NAN constant)
* INTEGER = 0

There are special operators defined to easily get the values depending on the lvalue(left hand-side value). So the following is possible:

```
Object o;
int integer = o;
double floating = o;
std::string text = o;
```

### SSQLite3 Object Assign Value ###

* void [Object::SetInt](#markdown-header-ssqlite3-object-assign-value)( const int& value )
* void [Object::SetReal](#markdown-header-ssqlite3-object-assign-value)( const double& value )
* void [Object::SetText](#markdown-header-ssqlite3-object-assign-value)( const std::string& value )
* void [Object::SetText](#markdown-header-ssqlite3-object-assign-value)( const char* value )
* void [Object::SetBlob](#markdown-header-ssqlite3-object-assign-value)( const char* value, const std::size_t& size )

* [Object](#markdown-header-ssqlite3-object-constructor)& [Object::operator =](#markdown-header-ssqlite3-object-assign-value) ( const [Object](#markdown-header-ssqlite3-object-constructor)& value )
* [Object](#markdown-header-ssqlite3-object-constructor)& [Object::operator =](#markdown-header-ssqlite3-object-assign-value) ( const int& value )
* [Object](#markdown-header-ssqlite3-object-constructor)& [Object::operator =](#markdown-header-ssqlite3-object-assign-value) ( const char * value )
* [Object](#markdown-header-ssqlite3-object-constructor)& [Object::operator =](#markdown-header-ssqlite3-object-assign-value) ( const std::string& value )
* [Object](#markdown-header-ssqlite3-object-constructor)& [Object::operator =](#markdown-header-ssqlite3-object-assign-value) ( const double& value )

All functions will set the value of the [Object](#markdown-header-ssqlite3-object-constructor) class. There is also an operator to easily copy from an other [Object](#markdown-header-ssqlite3-object-constructor) instance. To assign a value of NOTHING then you have to assign a new created instance of an [Object](#markdown-header-ssqlite3-object-constructor).

There are also assign operators so you easily type like this:

```
Object integer = 5;
Object floating = 5.7;
Object text = "this is how it works";
```

### SSQLite3 ObjectMap Iterator ###

* [ObjectMap::iterator](#markdown-header-ssqlite3-objectmap-iterator)
* [ObjectMap::const_iterator](#markdown-header-ssqlite3-objectmap-iterator)

* [ObjectMap::iterator](#markdown-header-ssqlite3-objectmap-iterator) [ObjectMap::begin](#markdown-header-ssqlite3-objectmap-iterator)( )
* [ObjectMap::const_iterator](#markdown-header-ssqlite3-objectmap-iterator) [ObjectMap::begin](#markdown-header-ssqlite3-objectmap-iterator)( ) const
* [ObjectMap::iterator](#markdown-header-ssqlite3-objectmap-iterator) [ObjectMap::end](#markdown-header-ssqlite3-objectmap-iterator)( )
* [ObjectMap::const_iterator](#markdown-header-ssqlite3-objectmap-iterator) [ObjectMap::end](#markdown-header-ssqlite3-objectmap-iterator)( ) const
* bool [ObjectMap::empty](#markdown-header-ssqlite3-objectmap-iterator)( ) const

The [ObjectMap](#markdown-header-objectmap] has typical STL-container-like functions like begin( ), end( ) and empty( ) to check and access the inner container. They can easily be used with any STL-algorithm, boost::algorithm function or for typical iteration.

### SSQLite3 ObjectMap Access ###

* [Object](#markdown-header-ssqlite3-object-constructor)& [ObjectMap::operator](#markdown-header-ssqlite3-objectmap-access)[]( const std::string& key )
* [Object](#markdown-header-ssqlite3-object-constructor)& [ObjectMap::at](#markdown-header-ssqlite3-objectmap-access)( const std::string& key )
* const [Object](#markdown-header-ssqlite3-object-constructor) [ObjectMap::operator](#markdown-header-ssqlite3-objectmap-access) [] ( const std::string& key ) const
* const [Object](#markdown-header-ssqlite3-object-constructor) [ObjectMap::at](#markdown-header-ssqlite3-objectmap-access)( const std::string& key ) const

Easy access to the [Object](#markdown-header-ssqlite3-object-constructor)s by their keys, but you better check if the key [Exists](#markdown-header-ssqlite3-objectmap-exists) otherwise you end up with a value of NOTHING.
The [()-operator](#markdown-header-ssqlite3-objectmap-exists) is for testing, [[]-operator](#markdown-header-ssqlite3-objectmap-access) for accessing.

```
ObjectMap map;
if ( map( "key" ) ) // test
    map[ "key" ]... // access
```

### SSQLite3 ObjectMap Exists ###

* bool [ObjectMap::operator](#markdown-header-ssqlite3-objectmap-exists) ( ) ( const std::string& key ) const
* bool [ObjectMap::Exists](#markdown-header-ssqlite3-objectmap-exists)( const std::string& key ) const

Easy testing whether the given key is used. The [()-operator](#markdown-header-ssqlite3-objectmap-exists) is for testing, [[]-operator](#markdown-header-ssqlite3-objectmap-access) for accessing.

```
ObjectMap map;
if ( map( "key" ) ) // test
    map[ "key" ]... // access
```

### SSQLite3 Statement Constructor ###

* [Statement::Statement](#markdown-header-ssqlite3-statement-constructor)( const std::string& base )
* [Statement](#markdown-header-ssqlite3-statement-constructor)& [Statement::operator <<](#markdown-header-ssqlite3-statement-constructor) ( const T& value )

With the constructor and the [<<-operator](#markdown-header-ssqlite3-statement-constructor) you can modify the query itself.
```
Statement stmt( "INSERT INTO" );
stmt << " foobar(KEY,VALUE,EXTRA)" << " VALUES( ?, ?, ? )";
```

### SSQLite3 Statement Bind ###

* [Statement](#markdown-header-ssqlite3-statement-constructor)& [Statement::operator <=](#markdown-header-ssqlite3-statement-bind) ( const T& value )
* [Statement](#markdown-header-ssqlite3-statement-constructor)& [Bind](#markdown-header-ssqlite3-statement-bind)( const char* value )
* [Statement](#markdown-header-ssqlite3-statement-constructor)& [Bind](#markdown-header-ssqlite3-statement-bind)( const std::string& value )
* [Statement](#markdown-header-ssqlite3-statement-constructor)& [Bind](#markdown-header-ssqlite3-statement-bind)( const int& value )
* [Statement](#markdown-header-ssqlite3-statement-constructor)& [Bind](#markdown-header-ssqlite3-statement-bind)( const double& value )
* [Statement](#markdown-header-ssqlite3-statement-constructor)& [Bind](#markdown-header-ssqlite3-statement-bind)( const Object& value )

The [Bind](#markdown-header-ssqlite3-statement-bind) functions and the [<=-operator](#markdown-header-ssqlite3-statement-bind) can be chained up to make easy one-line [Statement](#markdown-header-ssqlite3-statement-constructor)s:

```
Statement stmt( "INSERT INTO foobar(KEY,VALUE,EXTRA) VALUES( ?, ?, ? )" );
stmt.Bind( "foo" ) <= "bar" <= 42;
```

### SSQLite3 Result Iterator ###

* [Result::iterator](#markdown-header-ssqlite3-result-iterator)
* [Result::const_iterator](#markdown-header-ssqlite3-result-iterator)

* [Result::iterator](#markdown-header-ssqlite3-result-iterator) [Result::begin](#markdown-header-ssqlite3-result-iterator)( )
* [Result::const_iterator](#markdown-header-ssqlite3-result-iterator) [Result::begin](#markdown-header-ssqlite3-result-iterator)( ) const
* [Result::iterator](#markdown-header-ssqlite3-result-iterator) [Result::end](#markdown-header-ssqlite3-result-iterator)( )
* [Result::const_iterator](#markdown-header-ssqlite3-result-iterator) [Result::end](#markdown-header-ssqlite3-result-iterator)( ) const

The [Result](#markdown-header-result) has typical STL-container-like functions like begin( ) and end( ) to check and access the inner container. They can easily be used with any STL-algorithm, boost::algorithm function or for typical iteration.

### SSQLite3 Result Check ###

* [Result::operator](#markdown-header-ssqlite3-result-check) bool( ) const
* int [Result::AffectedRows](#markdown-header-ssqlite3-result-check)( ) const

For easy checking whether a query was successful just test the [Result](#markdown-header-result) instance itself. It will converted into a boolean that says whether it worked or not. To check how many rows have been affected in a normal DELETE/UPDATE/INSERT query you can check [AffectedRows](#markdown-header-ssqlite3-result-check).

### SSQLite3 Connection Connection ###

* [Connection::Connection](#markdown-header-ssqlite3-connection-connection)( )
* [Connection::~Connection](#markdown-header-ssqlite3-connection-connection)( )

* bool [Connection::Connect](#markdown-header-ssqlite3-connection-connection)( const std::string& file, const bool& write = true, const bool& create = true )
* bool [Connection::CreateInMemory](#markdown-header-ssqlite3-connection-connection)( )
* bool [Connection::IsConnected](#markdown-header-ssqlite3-connection-connection)( ) const
* bool [Connection::Disconnect](#markdown-header-ssqlite3-connection-connection)( const bool& forceZombie = false )

The [Connection](#markdown-header-ssqlite3-connection-connection) class is the core. It connects and disconnects(automatically on destruction) and is not copyable(due to private copy-constructor and assign-operator). It can connect to temporary databases in memory or to databases stored in files. By default the disconnect function is not in zombie-mode. Call Disconnect( true ) or the destructor to activate the zombie-mode-disconnect. In zombie-mode the database stays alive until every instance that is related to the internal database structure (like [Statements](#markdown-header-statement)) are destroyed too. That should not happen if you work in one thread only and not do something on your own.

### SSQLite3 Connection Log ###

* void [Connection::SetLogging](#markdown-header-ssqlite3-connection-log)( const bool& value )
* bool [Connection::IsLogging](#markdown-header-ssqlite3-connection-log)( ) const
* bool [Connection::HasMessage](#markdown-header-ssqlite3-connection-log)( ) const
* std::string [Connection::PopMessage](#markdown-header-ssqlite3-connection-log)( )
* std::string [Connection::PeekMessage](#markdown-header-ssqlite3-connection-log)( ) const

As this wrapper does not throw any errors instead it catches all the errors and put them on a stack. You can access the stack by enabling the log and pop all messages on the stack. It constists mainly out of [sqlite3 error messages](http://www.sqlite.org/c3ref/c_abort.html) so you can easily look them up.

### SSQLite3 Connection Execute ###

* Result [Connection::Execute](#markdown-header-ssqlite3-connection-execute)( const std::string& query )
* Result [Connection::Execute](#markdown-header-ssqlite3-connection-execute)( const Statement& query )

These functions execeute the given [Statement](#markdown-header-statement)s and return their [Result](#markdown-header-result). As you can just check a [Result](#markdown-header-result) instance you can also just write:

```
if ( Connection::Execute( "INSERT INTO foobar( KEY, VALUE ) VALUES( 'key', 'value' )" ) ) // test
```

## License ##

Copyright (c) 2014 Joshua Behrens

This software is provided 'as-is', without any express or implied
warranty. In no event will the authors be held liable for any damages
arising from the use of this software.

Permission is granted to anyone to use this software for any purpose,
including commercial applications, and to alter it and redistribute it
freely, subject to the following restrictions:

   1. The origin of this software must not be misrepresented; you must not
   claim that you wrote the original software. If you use this software
   in a product, an acknowledgment in the product documentation would be
   appreciated but is not required.

   2. Altered source versions must be plainly marked as such, and must not be
   misrepresented as being the original software.

   3. This notice may not be removed or altered from any source
   distribution.

Based on the zlib-license: [http://zlib.net/zlib_license.html](http://zlib.net/zlib_license.html)

### tl;dr ###

As long as you do NOT say you wrote it and NOT remove this hint, you can use it non- and commercially. But if anything fails I am not the one you should blame.