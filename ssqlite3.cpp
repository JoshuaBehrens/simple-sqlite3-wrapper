//  Copyright (c) 2014 Joshua Behrens
//
//  This software is provided 'as-is', without any express or implied
//  warranty. In no event will the authors be held liable for any damages
//  arising from the use of this software.
//
//  Permission is granted to anyone to use this software for any purpose,
//  including commercial applications, and to alter it and redistribute it
//  freely, subject to the following restrictions:
//
//     1. The origin of this software must not be misrepresented; you must not
//     claim that you wrote the original software. If you use this software
//     in a product, an acknowledgment in the product documentation would be
//     appreciated but is not required.
//
//     2. Altered source versions must be plainly marked as such, and must not be
//     misrepresented as being the original software.
//
//     3. This notice may not be removed or altered from any source
//     distribution.
//
//  Based on the zlib-license: http://zlib.net/zlib_license.html
//

#include <cstdio>
#include <cmath>
#include <cstring>

#include "ssqlite3.hpp"
#include "sqlite3.h"

// SSQLite3::Format*

std::string SSQLite3::Formatq( const std::string& str )
{
	char* retv = sqlite3_mprintf( "%q", str.c_str( ) );
	std::string ret( retv );
	sqlite3_free( retv );
	return ret;
}
std::string SSQLite3::FormatQ( const std::string& str )
{
	char* retv = sqlite3_mprintf( "%Q", str.c_str( ) );
	std::string ret( retv );
	sqlite3_free( retv );
	return ret;
}
std::string SSQLite3::Formatz( const std::string& str )
{
	char* retv = sqlite3_mprintf( "%z", str.c_str( ) );
	std::string ret( retv );
	sqlite3_free( retv );
	return ret;
}

// SSQLite3::Object
namespace SSQLite3
{
    struct blob
    {
        char* data;
        std::size_t size;
    };
}

SSQLite3::Object::Object( )
{
	type = SSQLite3::Object::NOTHING;
	data = 0;
}
SSQLite3::Object::Object( const SSQLite3::Object& value )
{
	type = SSQLite3::Object::NOTHING;
	data = 0;
	*this = value;
}
SSQLite3::Object::Object( const int& value )
{
	type = SSQLite3::Object::NOTHING;
	data = 0;
	SetInt( value );
}
SSQLite3::Object::Object( const double& value )
{
	type = SSQLite3::Object::NOTHING;
	data = 0;
	SetReal( value );
}
SSQLite3::Object::Object( const std::string& value )
{
	type = SSQLite3::Object::NOTHING;
	data = 0;
	SetText( value );
}
SSQLite3::Object::Object( const char* value, const std::size_t& size )
{
	type = SSQLite3::Object::NOTHING;
	data = 0;
	SetBlob( value, size );
}

int SSQLite3::Object::GetInt( ) const
{
	switch ( type )
	{
	case SSQLite3::Object::INTEGER:
		return *((int*)data);
	case SSQLite3::Object::REAL:
		return *((double*)data);
	case SSQLite3::Object::TEXT:
		{
			int read = 0;
			if ( sscanf( ((std::string*)data)->c_str( ), "%i", &read ) > 0 )
				return read;
		}
	default: return 0;
	}
}
double SSQLite3::Object::GetReal( ) const
{
	switch ( type )
	{
	case SSQLite3::Object::INTEGER:
		return *((int*)data);
	case SSQLite3::Object::REAL:
		return *((double*)data);
	case SSQLite3::Object::TEXT:
		{
			double read = 0;
			if ( sscanf( ((std::string*)data)->c_str( ), "%lf", &read ) > 0 )
				return read;
		}
	default: return NAN;
	}
}
std::string SSQLite3::Object::GetText( ) const
{
	switch ( type )
	{
	case SSQLite3::Object::INTEGER:
		{
			char buffer[32];
			sprintf( buffer, "%i", *((int*)data) );
			return std::string( buffer );
		}
	case SSQLite3::Object::REAL:
		{
			char buffer[32];
			sprintf( buffer, "%lf", *((double*)data) );
			return std::string( buffer );
		}
	case SSQLite3::Object::TEXT:
		return *((std::string*)data);
	default: return "";
	}
}
const char* SSQLite3::Object::GetBlob( ) const
{
	return ( type == SSQLite3::Object::BLOB ) ? ((SSQLite3::blob*)data)->data : 0;
}
std::size_t SSQLite3::Object::GetBlobSize( ) const
{
	return ( type == SSQLite3::Object::BLOB ) ? ((SSQLite3::blob*)data)->size : 0;
}

void SSQLite3::Object::SetInt( const int& value )
{
	dispose( );
	int* t = new int( value );
	data = (void*)t;
	type = SSQLite3::Object::INTEGER;
}
void SSQLite3::Object::SetReal( const double& value )
{
	dispose( );
	double* t = new double( value );
	data = (void*)t;
	type = SSQLite3::Object::REAL;
}
void SSQLite3::Object::SetText( const std::string& value )
{
	dispose( );
	std::string* t = new std::string( value );
	data = (void*)t;
	type = SSQLite3::Object::TEXT;
}
void SSQLite3::Object::SetText( const char* value )
{
	dispose( );
	std::string* t = new std::string( value );
	data = (void*)t;
	type = SSQLite3::Object::TEXT;
}
void SSQLite3::Object::SetBlob( const char* value, const std::size_t& size )
{
	dispose( );
	SSQLite3::blob* b = new SSQLite3::blob( );
	b->data = new char[ size ];
	b->size = size;
	memcpy( b->data, value, size );
	data = (void*)b;
	type = SSQLite3::Object::BLOB;
}

SSQLite3::Object& SSQLite3::Object::operator = ( const SSQLite3::Object& copy )
{
	dispose( );
	switch ( copy.GetType( ) )
	{
	case SSQLite3::Object::BLOB:
		SetBlob( copy.GetBlob( ), copy.GetBlobSize( ) );
		break;
	case SSQLite3::Object::NOTHING:
		type = SSQLite3::Object::NOTHING;
		data = 0;
		break;
	case SSQLite3::Object::TEXT:
		SetText( copy.GetText( ) );
		break;
	case SSQLite3::Object::REAL:
		SetReal( copy.GetReal( ) );
		break;
	case SSQLite3::Object::INTEGER:
		SetInt( copy.GetInt( ) );
		break;
	}
	return *this;
}

void SSQLite3::Object::dispose( )
{
	switch ( type )
	{
	case SSQLite3::Object::BLOB:
		{
			SSQLite3::blob* b = (SSQLite3::blob*)data;
			delete[] b->data;
			delete b;
			data = 0;
		}
		break;
	case SSQLite3::Object::INTEGER:
		{
			int* b = (int*)data;
			delete b;
			data = 0;
		}
		break;
	case SSQLite3::Object::REAL:
		{
			double* b = (double*)data;
			delete b;
			data = 0;
		}
		break;
	case SSQLite3::Object::TEXT:
		{
			std::string* b = (std::string*)data;
			delete b;
			data = 0;
		}
		break;
	case SSQLite3::Object::NOTHING:
		data = 0;
		break;
	}
	type = SSQLite3::Object::NOTHING;
}

// SSQLite3::ObjectMap

const SSQLite3::Object SSQLite3::ObjectMap::at( const std::string& key ) const
{
	SSQLite3::ObjectMap::const_iterator it( data.find( key ) );
	SSQLite3::Object ret;
	if ( it != end( ) )
		ret = it->second;
	return ret;
}

// SSQLite3::Connection

bool SSQLite3::Connection::Connect( const std::string& file, const bool& write, const bool& create )
{
	int flags = 0;
	if ( create && write )
		flags |= SQLITE_OPEN_CREATE;
	if ( write )
		flags |= SQLITE_OPEN_READWRITE;
	else
		flags = SQLITE_OPEN_READONLY;
	int ret = sqlite3_open_v2( file.c_str( ), (sqlite3**)&con, flags, 0 );

	AddError( ret );
	if ( ret == 0 )
	{
		connected = true;
		return true;
	}
	return false;
}
bool SSQLite3::Connection::CreateInMemory( )
{
	if ( connected )
		return false;
	int ret = sqlite3_open( 0, (sqlite3**)&con );
	AddError( ret );
	if ( ret == 0 )
	{
		connected = true;
		return true;
	}
	return false;
}
bool SSQLite3::Connection::Disconnect( const bool& forceZombie )
{
	if ( forceZombie )
	{
		int ret = sqlite3_close_v2( (sqlite3*)con );
		AddError( ret );
		if ( ret == 0 || ret == SQLITE_BUSY )
		{
			connected = false;
			return true;
		}
	}
	else
	{
		int ret = sqlite3_close( (sqlite3*)con );
		AddError( ret );
		if ( ret == 0 )
		{
			connected = false;
			return true;
		}
	}
	return false;
}

void SSQLite3::Connection::SetLogging( const bool& value )
{
	logit = value;
	if ( !value )
		log.clear( );
}
std::string SSQLite3::Connection::PopMessage( )
{
	std::string ret;
	if ( HasMessage( ) )
	{
		ret = log.front( );
		log.pop_front( );
	}
	return ret;
}
std::string SSQLite3::Connection::PeekMessage( ) const
{
	if ( HasMessage( ) )
		return log.front( );
	return "";
}
SSQLite3::Result SSQLite3::Connection::Execute( const SSQLite3::Statement& stmt )
{
	SSQLite3::Result result;
	result.works = false;
	result.rows = 0;
	if ( connected )
	{
		sqlite3_stmt* stat;
		int ret = sqlite3_prepare_v2( (sqlite3*)con, stmt.query.str( ).c_str( ), -1, &stat, 0 );
		AddError( ret );
		if ( ret == SQLITE_OK )
		{
			int nbinds( sqlite3_bind_parameter_count( stat ) ), bcount( 1 ), check( SQLITE_OK );
			if ( unsigned( nbinds ) == stmt.binds.size( ) )
			{
				for ( std::list< SSQLite3::Object >::const_iterator it( stmt.binds.begin( ) ) ; it != stmt.binds.end( ) ;
					  ++it, ++bcount, check = SQLITE_OK )
				{
					switch ( it->GetType( ) )
					{
					case SSQLite3::Object::BLOB:
						check = sqlite3_bind_blob( stat, bcount, it->GetBlob( ), it->GetBlobSize( ), 0 );
						break;
					case SSQLite3::Object::INTEGER:
						check = sqlite3_bind_int( stat, bcount, it->GetInt( ) );
						break;
					case SSQLite3::Object::REAL:
						check = sqlite3_bind_double( stat, bcount, it->GetReal( ) );
						break;
					case SSQLite3::Object::TEXT:
						check = sqlite3_bind_text( stat, bcount, it->GetText( ).c_str( ), it->GetText( ).size( ), 0 );
						break;
					case SSQLite3::Object::NOTHING:
						check = sqlite3_bind_null( stat, bcount );
						break;
					}
					AddError( check );
				}
				result.works = true;
				int cols = sqlite3_column_count( stat );
				int betares = sqlite3_step( stat );

				while ( betares == SQLITE_ROW )
				{
					SSQLite3::ObjectMap map;
					for ( int c( 0 ) ; c < cols ; ++c )
					{
						SSQLite3::Object o;
						switch ( sqlite3_column_type( stat, c ) )
						{
						case SQLITE_INTEGER:
							o.SetInt( sqlite3_column_int( stat, c ) );
							break;
						case SQLITE_FLOAT:
							o.SetReal( sqlite3_column_double( stat, c ) );
							break;
						case SQLITE_TEXT:
							o.SetText( std::string( (const char*)sqlite3_column_text( stat, c ) ) );
							break;
						case SQLITE_BLOB:
							o.SetBlob( (const char*)sqlite3_column_blob( stat, c ), sqlite3_column_bytes( stat, c ) );
							break;
						case SQLITE_NULL:
						default:
							break;
						}
						map[ std::string( sqlite3_column_name( stat, c ) ) ] = o;
					}
					result.results.push_back( map );
					betares = sqlite3_step( stat );
				};
				result.rows = sqlite3_changes( (sqlite3*)con );
			}
			else
				AddError( "Amount of available bind spots and bound parameters must be equal" );
		}
		sqlite3_finalize( stat );
	}
	return result;
}

// protected

void SSQLite3::Connection::AddError( const int& code )
{
	if ( code != SQLITE_OK )
		AddError( sqlite3_errmsg( (sqlite3*)con ) );
}