//  Copyright (c) 2014 Joshua Behrens
//
//  This software is provided 'as-is', without any express or implied
//  warranty. In no event will the authors be held liable for any damages
//  arising from the use of this software.
//
//  Permission is granted to anyone to use this software for any purpose,
//  including commercial applications, and to alter it and redistribute it
//  freely, subject to the following restrictions:
//
//     1. The origin of this software must not be misrepresented; you must not
//     claim that you wrote the original software. If you use this software
//     in a product, an acknowledgment in the product documentation would be
//     appreciated but is not required.
//
//     2. Altered source versions must be plainly marked as such, and must not be
//     misrepresented as being the original software.
//
//     3. This notice may not be removed or altered from any source
//     distribution.
//
//  Based on the zlib-license: http://zlib.net/zlib_license.html
//

#ifndef SSQLITE3_BIND_OPERATOR
    #define SSQLITE3_BIND_OPERATOR <=
#endif // SSQLITE3_BIND_OPERATOR

#ifndef SSQLITE3_EXTEND_OPERATOR
    #define SSQLITE3_EXTEND_OPERATOR <<
#endif // SSQLITE3_EXTEND_OPERATOR

#ifndef _SSQLITE3_HPP
	#define _SSQLITE3_HPP

	#include <string>
	#include <sstream>
	#include <list>
	#include <map>

namespace SSQLite3
{
	std::string Formatq( const std::string& str );
	std::string FormatQ( const std::string& str );
	std::string Formatz( const std::string& str );

	class Object
	{
	public:
		enum Type
		{
			REAL, INTEGER, BLOB, TEXT, NOTHING
		};

		Object( );
		Object( const Object& value );
		Object( const int& value );
		Object( const double& value );
		Object( const std::string& value );
		Object( const char* value, const std::size_t& size );
		inline ~Object( ) { dispose( ); }

		inline Type GetType( ) const { return type; }
		int GetInt( ) const;
		double GetReal( ) const;
		std::string GetText( ) const;
		const char* GetBlob( ) const;
		std::size_t GetBlobSize( ) const;

		void SetInt( const int& value );
		void SetReal( const double& value );
		void SetText( const std::string& value );
		void SetText( const char* value );
		void SetBlob( const char* value, const std::size_t& size );

		operator int( ) const { return GetInt( ); }
		operator std::string( ) const { return GetText( ); }
		operator double( ) const { return GetReal( ); }

		Object& operator = ( const Object& value );
		inline Object& operator = ( const int& value ) { SetInt( value ) ; return *this; }
		inline Object& operator = ( const char * value ) { SetText( value ) ; return *this; }
		inline Object& operator = ( const std::string& value ) { SetText( value ) ; return *this; }
		inline Object& operator = ( const double& value ) { SetReal( value ) ; return *this; }

		inline bool IsInt( )  const { return type == INTEGER; }
		inline bool IsReal( ) const { return type == REAL; }
		inline bool IsText( ) const { return type == TEXT; }
		inline bool IsBlob( ) const { return type == BLOB; }
		inline bool IsNothing( ) const { return type == NOTHING; }
	private:
		void dispose( );

		Type type;
		void* data;
	};

	class ObjectMap
	{
	public:
		typedef std::map< std::string, Object >::iterator iterator;
		typedef std::map< std::string, Object >::const_iterator const_iterator;

		inline iterator begin( ) { return data.begin( ); }
		inline const_iterator begin( ) const { return data.begin( ); }
		inline iterator end( ) { return data.end( ); }
		inline const_iterator end( ) const { return data.end( ); }
		inline bool empty( ) const { return data.empty( ); }

		inline Object& operator [] ( const std::string& key ) { return at( key ); }
		inline Object& at( const std::string& key ) { return data[ key ]; }

		inline const Object operator [] ( const std::string& key ) const { return at( key ); }
		const Object at( const std::string& key ) const;

		inline bool operator () ( const std::string& key ) const { return Exists( key); }
		inline bool Exists( const std::string& key ) const { return data.find( key ) != data.end( ); }
	private:
		std::map< std::string, Object > data;
	};

	class Statement
	{
	public:
		inline Statement( const std::string& base ) { query << base; }

		template < typename T > inline Statement& operator SSQLITE3_EXTEND_OPERATOR ( const T& value ) { query << value; return *this ; }
		template < typename T > inline Statement& operator SSQLITE3_BIND_OPERATOR ( const T& value ) { return Bind( value ); }

		inline Statement& Bind( const char* value ) { return Bind( Object( std::string( value ) ) ); }
		inline Statement& Bind( const std::string& value ) { return Bind( Object( value ) ); }
		inline Statement& Bind( const int& value ) { return Bind( Object( value ) ); }
		inline Statement& Bind( const double& value ) { return Bind( Object( value ) ); }
		inline Statement& Bind( const Object& value ) { binds.push_back( value ); return *this; }
	private:
		std::ostringstream query;
		std::list< Object > binds;

		friend class Connection;
	};

	class Result
	{
	public:
		typedef std::list< ObjectMap >::iterator iterator;
		typedef std::list< ObjectMap >::const_iterator const_iterator;

		inline iterator begin( ) { return results.begin( ); }
		inline const_iterator begin( ) const { return results.begin( ); }
		inline iterator end( ) { return results.end( ); }
		inline const_iterator end( ) const { return results.end( ); }

		inline operator bool( ) const { return works; }
		inline int AffectedRows( ) const { return rows; }
	private:
		std::list< ObjectMap > results;
		bool works;
		int rows;

		friend class Connection;
	};

	class Connection
	{
	public:
		inline Connection( ) { con = 0; logit = false; }
		inline ~Connection( ) { Disconnect( true ); }

		bool Connect( const std::string& file, const bool& write = true, const bool& create = true );
		bool CreateInMemory( );
		bool IsConnected( ) const { return connected; }
		bool Disconnect( const bool& forceZombie = false );

		void SetLogging( const bool& value );
		inline bool IsLogging( ) const { return logit; }
		inline bool HasMessage( ) const { return !log.empty( ); }
		std::string PopMessage( );
		std::string PeekMessage( ) const;

		inline Result Execute( const std::string& query ) { return Execute( Statement( query ) ); }
		Result Execute( const Statement& query );
	protected:
		void AddError( const int& code );
		inline void AddError( const std::string& msg ) { if ( logit ) log.push_back( msg ); }
		void* con;
	private:
		bool logit, connected;
		std::list< std::string > log;

		inline Connection( const Connection& ) { }
		inline Connection& operator = ( const Connection& ) { return *this; }
	};
}

#endif // _SSQLITE3_HPP